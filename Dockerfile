FROM openjdk:17
WORKDIR /app
COPY jarfile/springboot-backend-0.0.1-SNAPSHOT.jar /app
EXPOSE 2375
CMD ["java", "-jar", "springboot-backend-0.0.1-SNAPSHOT.jar"]
